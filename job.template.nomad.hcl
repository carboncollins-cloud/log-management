job "log-management" {
  name        = "Log Management (Loki)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-monitoring"

  group "loki" {
    count = 1

    consul {}

    volume "loki-data" {
      type            = "csi"
      source          = "loki-data"
      attachment_mode = "file-system"
      access_mode     = "single-node-writer"
      per_alloc       = true
    }

    network {
      mode = "bridge"
    }

    service {
      provider = "consul"
      name     = "loki"
      port     = "3100"
      task     = "loki"

      connect {
        sidecar_service {}
      }

      check {
        expose   = true
        name     = "Application Ready Status"
        type     = "http"
        path     = "/ready"
        interval = "10s"
        timeout  = "3s"
      }
    }

    task "loki" {
      driver = "docker"
      user   = "[[ .defaultUserId ]]"

      config {
        image = "[[ .lokiImageName ]]"

        volumes = [
          "local/loki/local-config.yaml:/etc/loki/local-config.yaml"
        ]
      }

      volume_mount {
        volume      = "loki-data"
        destination = "/loki"
        read_only   = false
      }

      template {
        data = <<EOH
[[ fileContents "./config/loki.template.yaml" ]]
        EOH

        destination = "local/loki/local-config.yaml"
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
  }
}
