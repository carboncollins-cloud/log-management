terraform {
  required_providers {
    consul = {
      source = "hashicorp/consul"
      version = "2.20.0"
    }
  }
}

resource "consul_config_entry" "loki_intention" {
  name = "loki"
  kind = "service-intentions"

  config_json = jsonencode({
    Sources = [
      {
        Action     = "allow"
        Name       = "grafana"
        Precedence = 9
        Type       = "consul"
      },
      {
        Action     = "allow"
        Name       = "promtail"
        Precedence = 9
        Type       = "consul"
      },
      {
        Action     = "allow"
        Name       = "internal-proxy"
        Precedence = 9
        Type       = "consul"
      }
    ]
  })
}
