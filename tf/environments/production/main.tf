terraform {
  backend "consul" {
    path = "terraform/monitoring-log-management"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "2.1.0"
    }

    consul = {
      source = "hashicorp/consul"
      version = "2.20.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.14.0"
    }
  }
}

provider "nomad" {}
provider "consul" {}
provider "vault" {}

module "intentions" {
  source = "../../modules/lokiConsulIntentions"
}

module "soc_volumes" {
  source = "../../modules/lokiNomadVolumes"

  plugin_id = "soc-axion-smb"

  cifs_user_id = 3205
  cifs_group_id = 3205
}
