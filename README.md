# Log Management

[[_TOC_]]

## Description

Log management using [Loki](https://grafana.com/logs/) to centralise logging between all services and servers running within the [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) project.

## Other Info

I am still evaluating this service so configurations and setup may be subject to change
